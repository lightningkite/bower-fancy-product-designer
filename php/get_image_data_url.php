<?php

//Returns the data url of an image

$url = trim($_POST['url']);

if(!function_exists('getimagesize')) {
	echo json_encode(array('error' => 'The php function getimagesize is not installed on your server. Please contact your server provider!'));
	die;
}

if(!function_exists('curl_version')) {
	echo json_encode(array('error' => 'cURL is not enabled on your web server, please enable it!'));
	die;
}


$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
$result = curl_exec($ch);
curl_close($ch);

if($result == false) {
	$result = file_get_contents($url);
}

$info = getimagesize($url);
$data_url =  'data: '.$info['mime'].';base64,'.base64_encode($result);
echo json_encode(array( 'data_url' => $data_url));

?>